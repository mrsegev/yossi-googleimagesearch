package com.yossisegev.yossisegevdemoapp;
import com.yossisegev.yossisegevdemoapp.entities.ImageResult;
import com.yossisegev.yossisegevdemoapp.model.db.ImageDB;

import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, manifest = "/src/main/AndroidManifest.xml")
public class DBTests {

    @Test
    public void testInsert() {

        String queryId = "test110";
        List<ImageResult> imageResults = new ArrayList<>();
        imageResults.add(ImageResult.mock("1"));
        imageResults.add(ImageResult.mock("2"));
        imageResults.add(ImageResult.mock("3"));
        imageResults.add(ImageResult.mock("4"));
        imageResults.add(ImageResult.mock("5"));
        ImageDB.getInstance().insert(queryId, imageResults);
        List<ImageResult> stored = ImageDB.getInstance().get(queryId);
        assertTrue(stored.size() == imageResults.size());

        imageResults.clear();
        imageResults.add(ImageResult.mock("1"));
        imageResults.add(ImageResult.mock("2"));
        ImageDB.getInstance().insert(queryId, imageResults);
        List<ImageResult> stored2 = ImageDB.getInstance().get(queryId);
        assertTrue(stored2.size() == imageResults.size());
    }

    @Test
    public void testInsertDifferentQuery() {
        String queryId = "test110";
        String anotherQueryId = "anothertest110";
        List<ImageResult> imageResults = new ArrayList<>();
        imageResults.add(ImageResult.mock("1"));
        imageResults.add(ImageResult.mock("2"));
        imageResults.add(ImageResult.mock("3"));
        imageResults.add(ImageResult.mock("4"));
        imageResults.add(ImageResult.mock("5"));
        ImageDB.getInstance().insert(queryId, imageResults);

        imageResults.clear();
        imageResults.add(ImageResult.mock("1"));
        imageResults.add(ImageResult.mock("2"));
        ImageDB.getInstance().insert(anotherQueryId, imageResults);
        List<ImageResult> storedFirst = ImageDB.getInstance().get(queryId);
        assertTrue(storedFirst.size() == 5);
        List<ImageResult> storedSecond = ImageDB.getInstance().get(anotherQueryId);
        assertTrue(storedSecond.size() == 2);
    }
}