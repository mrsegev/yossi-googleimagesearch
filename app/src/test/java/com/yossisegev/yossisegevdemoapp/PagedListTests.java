package com.yossisegev.yossisegevdemoapp;


import com.yossisegev.yossisegevdemoapp.common.PagedImageList;
import com.yossisegev.yossisegevdemoapp.entities.ImageResult;
import com.yossisegev.yossisegevdemoapp.entities.QueryIdentifier;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PagedListTests {

    private PagedImageList pagedImageList;

    @Before
    public void setup() {
        pagedImageList = new PagedImageList();
    }

    @Test
    public void testInsertNewPageToEmptyList() {
        QueryIdentifier qid = new QueryIdentifier("test", 1, 10);
        List<ImageResult> imageResults = getImageBulk(5);
        pagedImageList.updatePage(qid, imageResults);
        assertTrue(pagedImageList.size() == imageResults.size());
        assertTrue(pagedImageList.containsQuery(qid));
        assertTrue(pagedImageList.getStartPosition(qid) == 0);
        assertTrue(pagedImageList.getEndPosition(qid) == imageResults.size() - 1);
    }

    @Test
    public void testInsertNewPageOnPopulatedList() {
        QueryIdentifier qid = new QueryIdentifier("test", 1, 10);
        QueryIdentifier qid2 = new QueryIdentifier("test", 11, 10);
        QueryIdentifier qid3 = new QueryIdentifier("test", 21, 10);
        List<ImageResult> imagesPage1 = getImageBulk(5);
        pagedImageList.updatePage(qid, imagesPage1);
        List<ImageResult> imagesPage2 = getImageBulk(5);
        pagedImageList.updatePage(qid2, imagesPage2);
        List<ImageResult> imagesPage3 = getImageBulk(5);
        pagedImageList.updatePage(qid3, imagesPage3);
        assertTrue(pagedImageList.size() == (imagesPage1.size() + imagesPage2.size() + imagesPage3.size()));
        assertTrue(pagedImageList.getStartPosition(qid) == 0);
        assertTrue(pagedImageList.getEndPosition(qid) == 4);
        assertTrue(pagedImageList.getStartPosition(qid2) == 5);
        assertTrue(pagedImageList.getEndPosition(qid2) == 9);
        assertTrue(pagedImageList.getStartPosition(qid3) == 10);
        assertTrue(pagedImageList.getEndPosition(qid3) == 14);
    }

    @Test
    public void testUpdateExistingPageMidList() {
        QueryIdentifier qid = new QueryIdentifier("test", 1, 10);
        QueryIdentifier qid2 = new QueryIdentifier("test", 11, 10);
        QueryIdentifier qid3 = new QueryIdentifier("test", 21, 10);
        List<ImageResult> imagesPage1 = getImageBulk(7);
        pagedImageList.updatePage(qid, imagesPage1);
        List<ImageResult> imagesPage2 = getImageBulk(20);
        pagedImageList.updatePage(qid2, imagesPage2);
        List<ImageResult> imagesPage3 = getImageBulk(12);
        pagedImageList.updatePage(qid3, imagesPage3);

        assertTrue(pagedImageList.size() == (imagesPage1.size() + imagesPage2.size() + imagesPage3.size()));
        ImageResult firstPage2 = pagedImageList.get(pagedImageList.getStartPosition(qid2));
        ImageResult lastPage2 = pagedImageList.get(pagedImageList.getEndPosition(qid2));
        assertTrue(firstPage2.getId().equals("0"));
        assertTrue(lastPage2.getId().equals("19"));

        List<ImageResult> newImageResults = new ArrayList<>();
        newImageResults.add(ImageResult.mock("A"));
        newImageResults.add(ImageResult.mock("B"));
        newImageResults.add(ImageResult.mock("C"));
        pagedImageList.updatePage(qid2, newImageResults);

        assertTrue(pagedImageList.size() == (imagesPage1.size() + newImageResults.size() + imagesPage3.size()));
        ImageResult firstNewPage2 = pagedImageList.get(pagedImageList.getStartPosition(qid2));
        ImageResult lastNewPage2 = pagedImageList.get(pagedImageList.getEndPosition(qid2));
        assertTrue(firstNewPage2.getId().equals("A"));
        assertTrue(lastNewPage2.getId().equals("C"));
    }

    @Test
    public void testUpdateExistingPageEndOfList() {
        QueryIdentifier qid = new QueryIdentifier("test", 1, 10);
        QueryIdentifier qid2 = new QueryIdentifier("test", 11, 10);
        QueryIdentifier qid3 = new QueryIdentifier("test", 21, 10);
        List<ImageResult> imagesPage1 = getImageBulk(7);
        pagedImageList.updatePage(qid, imagesPage1);
        List<ImageResult> imagesPage2 = getImageBulk(20);
        pagedImageList.updatePage(qid2, imagesPage2);
        List<ImageResult> imagesPage3 = getImageBulk(12);
        pagedImageList.updatePage(qid3, imagesPage3);

        assertTrue(pagedImageList.size() == (imagesPage1.size() + imagesPage2.size() + imagesPage3.size()));
        ImageResult firstPage3 = pagedImageList.get(pagedImageList.getStartPosition(qid3));
        ImageResult lastPage3 = pagedImageList.get(pagedImageList.getEndPosition(qid3));
        assertTrue(firstPage3.getId().equals("0"));
        assertTrue(lastPage3.getId().equals("11"));

        List<ImageResult> newImageResults = new ArrayList<>();
        newImageResults.add(ImageResult.mock("A"));
        newImageResults.add(ImageResult.mock("B"));
        newImageResults.add(ImageResult.mock("C"));
        pagedImageList.updatePage(qid3, newImageResults);
        assertTrue(pagedImageList.size() == (imagesPage1.size() + newImageResults.size() + imagesPage2.size()));
        ImageResult firstNewPage3 = pagedImageList.get(pagedImageList.getStartPosition(qid3));
        ImageResult lastNewPage3 = pagedImageList.get(pagedImageList.getEndPosition(qid3));
        assertTrue(firstNewPage3.getId().equals("A"));
        assertTrue(lastNewPage3.getId().equals("C"));
    }

    @Test
    public void testClear() {
        QueryIdentifier qid = new QueryIdentifier("test", 1, 10);
        List<ImageResult> imageResults = getImageBulk(15);
        pagedImageList.updatePage(qid, imageResults);
        assertTrue(pagedImageList.size() == imageResults.size());
        assertTrue(pagedImageList.containsQuery(qid));
        pagedImageList.clear();
        assertTrue(pagedImageList.size() == 0);
        assertFalse(pagedImageList.containsQuery(qid));
    }

    private List<ImageResult> getImageBulk(int count) {
        List<ImageResult> imageResults = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            imageResults.add(ImageResult.mock(Integer.toString(i)));
        }
        return imageResults;
    }
}