package com.yossisegev.yossisegevdemoapp.common;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

/**
 * Created by Yossi on 12/14/16.
 */

public class App extends Application {
    @SuppressLint("StaticFieldLeak")
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static Context getContext() {
        return context;
    }
}
