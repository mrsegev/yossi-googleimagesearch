package com.yossisegev.yossisegevdemoapp.common;


import com.yossisegev.yossisegevdemoapp.entities.ImageResult;
import com.yossisegev.yossisegevdemoapp.entities.QueryIdentifier;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Yossi on 12/14/16.
 */

public class PagedImageList {

    private List<ImageResult> imageResults;
    private LinkedHashMap<QueryIdentifier, Integer> pagesMap;
    private final Object lock = new Object();

    public PagedImageList() {
        imageResults = new ArrayList<>();
        pagesMap = new LinkedHashMap<>();
    }

    public ImageResult get(int position) {
        synchronized (lock) {
            return imageResults.get(position);
        }
    }

    public int size() {
        synchronized (lock) {
            return imageResults.size();
        }
    }

    public boolean containsQuery(QueryIdentifier queryIdentifier) {
        return pagesMap.containsKey(queryIdentifier);
    }

    public void clear() {
        synchronized (lock) {
            imageResults.clear();
            pagesMap.clear();
        }
    }

    public void updatePage(QueryIdentifier qid, List<ImageResult> newImageResults) {
        synchronized (lock) {

            int addToIndex;
            int recordsCountDiff = 0;
            // If Page already exists:
            // Remove the related Images and insert newImages.
            // If new page:
            // Just add to existing list
            if (pagesMap.containsKey(qid)) {
                int pageFirstPos = getStartPosition(qid);
                int pageLastPos = getEndPosition(qid);
                recordsCountDiff = newImageResults.size() - ((pageLastPos+1)-pageFirstPos);

                Iterator it = imageResults.listIterator(pageFirstPos);
                int pos = pageFirstPos;
                while (it.hasNext() && pos <= pageLastPos) {
                    it.next();
                    it.remove();
                    pos++;
                }
                addToIndex = pageFirstPos;


            } else {
                addToIndex = imageResults.size();
            }

            pagesMap.put(qid, addToIndex);
            imageResults.addAll(addToIndex, newImageResults);
            refreshIndexs(qid.getNextIdentifier(), recordsCountDiff);

        }
    }

    private void refreshIndexs(QueryIdentifier fromQid, int recordsCountDiff) {
        if (pagesMap.containsKey(fromQid) && recordsCountDiff != 0) {
            int currentPos = pagesMap.get(fromQid);
            pagesMap.put(fromQid, currentPos + recordsCountDiff);
            refreshIndexs(fromQid.getNextIdentifier(), recordsCountDiff);
        }
    }

    public int getStartPosition(QueryIdentifier queryIdentifier) {
        if (pagesMap.containsKey(queryIdentifier)) {
            return pagesMap.get(queryIdentifier);
        } else {
            return -1;
        }
    }

    public int getEndPosition(QueryIdentifier qid) {
        if (pagesMap.containsKey(qid)) {
            return pagesMap.containsKey(qid.getNextIdentifier()) ? (pagesMap.get(qid.getNextIdentifier())-1): imageResults.size() - 1;
        } else {
            return -1;
        }
    }
}
