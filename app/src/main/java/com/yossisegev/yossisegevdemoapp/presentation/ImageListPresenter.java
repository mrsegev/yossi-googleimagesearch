package com.yossisegev.yossisegevdemoapp.presentation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;

import com.yossisegev.yossisegevdemoapp.common.App;
import com.yossisegev.yossisegevdemoapp.entities.ImageSearchResult;
import com.yossisegev.yossisegevdemoapp.view.ImageListView;

/**
 * Created by Yossi Segev on 14/12/2016.
 */

public class ImageListPresenter extends BasePresenter<ImageListView>
        implements LoaderManager.LoaderCallbacks<ImageSearchResult> {

    private LoaderManager loaderManager;
    private boolean loading;
    private int startIndex;
    private String searchTerm;

    private static class SingletonHolder {
        static final ImageListPresenter INSTANCE = new ImageListPresenter();
    }

    public static ImageListPresenter getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void setLoaderManager(@NonNull LoaderManager loaderManager) {
        this.loaderManager = loaderManager;
    }

    public void search(String searchTerm) {
        if (!TextUtils.isEmpty(searchTerm)) {
            this.startIndex = 1;
            this.searchTerm = searchTerm;
            loading = false;
            if (attached()) {
                view().clearList();
                view().showLoading(true);
            }
            loaderManager.restartLoader(ImageSearchLoader.ID, null, this);
        }
    }

    public void next() {
        if (!loading) {
            loaderManager.restartLoader(ImageSearchLoader.ID, null, this);
        }
    }

    @Override
    public Loader<ImageSearchResult> onCreateLoader(int id, Bundle args) {
        ImageSearchLoader loader = new ImageSearchLoader(App.getContext(), startIndex, searchTerm);
        loading = true;
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<ImageSearchResult> loader, ImageSearchResult data) {
        if (data == null) {
            if (attached()) {
                view().showLoading(false);
                view().onError();
            }
            return;
        }
        if (data.getSource() == ImageSearchResult.SOURCE_API) {
            loading = false;
            startIndex = data.getNextIndex();
        }
        if (attached()) {
            view().showLoading(false);
            view().onImageResponseData(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<ImageSearchResult> loader) {
        loading = false;
    }

    @Override
    protected void onViewDetaching(ImageListView view) {
        super.onViewDetaching(view);
        loaderManager = null;
    }
}
