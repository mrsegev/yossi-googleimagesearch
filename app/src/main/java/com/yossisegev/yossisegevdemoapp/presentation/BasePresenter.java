package com.yossisegev.yossisegevdemoapp.presentation;


import com.yossisegev.yossisegevdemoapp.view.BaseView;

import java.lang.ref.WeakReference;


/**
 * Created by Yossi Segev on 12/14/16.
 */
public abstract class BasePresenter<V extends BaseView> {

    private WeakReference<V> viewWeakReference;

    public void attachView(V view) {
        viewWeakReference = new WeakReference<>(view);
        onViewAttached(viewWeakReference.get());
    }

    public void detachView() {
        if (viewWeakReference != null) {
            onViewDetaching(viewWeakReference.get());
            viewWeakReference.clear();
            viewWeakReference = null;
        }
    }

    protected V view() {
        if (viewWeakReference != null) {
            return viewWeakReference.get();
        }
        return null;
    }

    public boolean attached() {
        return viewWeakReference != null && viewWeakReference.get() != null;
    }

    protected void onViewAttached(V view) {
    }

    protected void onViewDetaching(V view) {
    }
}
