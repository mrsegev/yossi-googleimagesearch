package com.yossisegev.yossisegevdemoapp.presentation;


import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.yossisegev.yossisegevdemoapp.entities.ImageResult;
import com.yossisegev.yossisegevdemoapp.entities.ImageSearchResult;
import com.yossisegev.yossisegevdemoapp.entities.QueryIdentifier;
import com.yossisegev.yossisegevdemoapp.model.api.ImageSearchApiResponse;
import com.yossisegev.yossisegevdemoapp.model.api.ImageSearchService;
import com.yossisegev.yossisegevdemoapp.model.db.ImageDB;

import java.util.List;

/**
 * Created by Yossi on 12/14/16.
 */

public class ImageSearchLoader extends AsyncTaskLoader<ImageSearchResult> {

    private static final String TAG = "ImageSearchLoader";
    public static final int ID = 100;
    private int startIndex;
    private String searchTerm;

    public ImageSearchLoader(Context context, int startIndex, String searchTerm) {
        super(context);
        this.searchTerm = searchTerm;
        this.startIndex = startIndex;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public ImageSearchResult loadInBackground() {
        Log.d(TAG, "loadInBackground: fetch from index: " + startIndex);

        // Return results from DB
        final QueryIdentifier qid = new QueryIdentifier(searchTerm, startIndex, ImageSearchService.getResultsCount());
        final List<ImageResult> oldImageResults = ImageDB.getInstance().get(qid.getId());
        if (oldImageResults != null && !oldImageResults.isEmpty()) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    ImageSearchResult data = new ImageSearchResult(ImageSearchResult.SOURCE_DB, qid, oldImageResults);
                    deliverResult(data);
                }
            });
        }

        // Refresh form api
        ImageSearchService imageSearchService = new ImageSearchService();
        ImageSearchApiResponse response = imageSearchService.search(startIndex, searchTerm);
        if (response == null) {
            return null;
        }
        QueryIdentifier queryIdentifier = response.getQuery().getIdentifier();

        // Update DB
        ImageDB.getInstance().insert(queryIdentifier.getId(), response.getImageResults());

        // Return updated results
        List<ImageResult> newImageResults = ImageDB.getInstance().get(queryIdentifier.getId());
        ImageSearchResult result = new ImageSearchResult(ImageSearchResult.SOURCE_API, queryIdentifier, newImageResults);
        result.setNextIndex(response.getQuery().getNextRequest().getStartIndex());
        return result;
    }
}
