package com.yossisegev.yossisegevdemoapp.model.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Yossi on 12/14/16.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final String TAG = "CityDBHelper";
    private static final String DB_NAME = "test_db";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        String CREATE_TBL = "CREATE TABLE IF NOT EXISTS " + ImageDB.IMAGE_TABLE
                + "("
                + ImageDB.ID + " integer primary key autoincrement, "
                + ImageDB.QUERY + " text not null, "
                + ImageDB.TITLE + " text not null, "
                + ImageDB.LINK + " text not null, "
                + ImageDB.THUMBNAIL + " text not null "
                + ");";

        db.execSQL(CREATE_TBL);
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
