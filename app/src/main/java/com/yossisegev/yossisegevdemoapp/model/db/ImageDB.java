package com.yossisegev.yossisegevdemoapp.model.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.yossisegev.yossisegevdemoapp.common.App;
import com.yossisegev.yossisegevdemoapp.entities.ImageResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yossi on 12/14/16.
 */

public class ImageDB {

    public static final String IMAGE_TABLE = "image_tbl";
    public static final String ID = "_id";
    public static final String QUERY = "query";
    public static final String TITLE = "title";
    public static final String LINK = "link";
    public static final String THUMBNAIL = "thumbnail";

    private DBHelper dbHelper;

    private static class SingletonHolder {
        static final ImageDB INSTANCE = new ImageDB();
    }

    private ImageDB() {
        dbHelper = new DBHelper(App.getContext());
    }

    public static ImageDB getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public List<ImageResult> get(String queryId) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] args = {queryId};
        Cursor c = db.query(IMAGE_TABLE, null, QUERY + " = ? ", args, null, null, null);
        List<ImageResult> imageResults = new ArrayList<>();
        while (c.moveToNext()) {
            ImageResult imageResult = new ImageResult();
            imageResult.setTitle(c.getString(c.getColumnIndex(TITLE)));
            imageResult.setLink(c.getString(c.getColumnIndex(LINK)));
            imageResult.setThumbnail(c.getString(c.getColumnIndex(THUMBNAIL)));
            imageResults.add(imageResult);
        }
        c.close();
        db.close();
        return imageResults;
    }

    public void insert(String queryId, List<ImageResult> imageResults) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String[] args = {queryId};
        db.beginTransaction();
        db.delete(IMAGE_TABLE, QUERY + " = ? ", args);

        for (ImageResult imageResult : imageResults) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(QUERY, queryId);
            contentValues.put(TITLE, imageResult.getTitle());
            contentValues.put(LINK, imageResult.getLink());
            contentValues.put(THUMBNAIL, imageResult.getThumbnail());
            db.insert(IMAGE_TABLE, null, contentValues);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }


}
