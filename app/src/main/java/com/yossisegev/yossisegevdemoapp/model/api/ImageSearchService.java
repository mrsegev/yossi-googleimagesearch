package com.yossisegev.yossisegevdemoapp.model.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Yossi on 12/14/16.
 */

public class ImageSearchService extends BaseService{

    private static final int RESULTS_COUNT = 10;
    private static final String SEARCH_TYPE = "image";
    private Api api;

    public ImageSearchService() {
        api = getRetrofit().create(Api.class);
    }

    public ImageSearchApiResponse search(int startIndex, String query) {
        Call<ImageSearchApiResponse> call = api.search(startIndex, query);
        return executeCall(call);
    }

    private interface Api {
        @GET("/customsearch/v1" +
                "?key=" + Statics.API_KEY +
                "&searchType=" + SEARCH_TYPE +
                "&num=" + RESULTS_COUNT)

        Call<ImageSearchApiResponse> search(@Query("start") int startIndex, @Query("q") String query);
    }

    public static int getResultsCount() {
        return RESULTS_COUNT;
    }
}
