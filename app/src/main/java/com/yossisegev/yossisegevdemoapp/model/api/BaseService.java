package com.yossisegev.yossisegevdemoapp.model.api;


import java.io.IOException;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yossi Segev on 12/14/2016.
 */

class BaseService {

    private Retrofit retrofit;

    BaseService() {
        retrofit = new Retrofit.Builder()
                .client(new OkHttpClient())
                .baseUrl(Statics.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    protected Retrofit getRetrofit() {
        return retrofit;
    }

    protected <T> T executeCall(Call<T> call) {
        try {
            Response<T> response = call.execute();
            if (response.isSuccessful()) {
                return response.body();
            } else {
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
