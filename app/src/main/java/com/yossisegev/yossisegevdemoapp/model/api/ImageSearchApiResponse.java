package com.yossisegev.yossisegevdemoapp.model.api;

import com.google.gson.annotations.SerializedName;
import com.yossisegev.yossisegevdemoapp.entities.ImageResult;
import com.yossisegev.yossisegevdemoapp.entities.Query;

import java.util.List;



/**
 * Created by Yossi on 12/14/16.
 */

public class ImageSearchApiResponse {
    @SerializedName("items")
    private List<ImageResult> imageResults;
    @SerializedName("queries")
    private Query query;

    public List<ImageResult> getImageResults() {
        return imageResults;
    }

    public Query getQuery() {
        return query;
    }
}
