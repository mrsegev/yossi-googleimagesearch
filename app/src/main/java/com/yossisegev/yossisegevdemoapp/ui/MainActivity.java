package com.yossisegev.yossisegevdemoapp.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.yossisegev.yossisegevdemoapp.R;
import com.yossisegev.yossisegevdemoapp.entities.ImageResult;
import com.yossisegev.yossisegevdemoapp.entities.ImageSearchResult;
import com.yossisegev.yossisegevdemoapp.presentation.ImageListPresenter;
import com.yossisegev.yossisegevdemoapp.view.ImageListView;

public class MainActivity extends AppCompatActivity implements ImageListView, PagedRecyclerView.PagingListener, ImageAdapter.Listener {

    private EditText searchEditText;
    private Button searchButton;
    private ProgressBar progressBar;
    private PagedRecyclerView recyclerView;
    private ImageAdapter adapter;
    private ImageListPresenter imageListPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        searchEditText = (EditText) findViewById(R.id.activity_main_edit_text);
        progressBar = (ProgressBar) findViewById(R.id.activity_main_progress_bar);
        searchButton = (Button) findViewById(R.id.activity_main_search_button);
        recyclerView = (PagedRecyclerView) findViewById(R.id.activity_main_recycler_view);
        recyclerView.setPagingListener(this);
        adapter = new ImageAdapter(this, this);
        recyclerView.setAdapter(adapter);
        imageListPresenter = ImageListPresenter.getInstance();

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageListPresenter != null) {
                    imageListPresenter.search(searchEditText.getText().toString());
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (imageListPresenter != null) {
            imageListPresenter.setLoaderManager(getSupportLoaderManager());
            imageListPresenter.attachView(this);
        }
    }

    @Override
    public void onError() {
        Toast.makeText(this, R.string.general_error_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void clearList() {
        adapter.clear();
    }

    @Override
    public void onImageResponseData(ImageSearchResult data) {
        /*if (data.getSource() == ImageSearchResult.SOURCE_API) {
            Toast.makeText(this, "Api results", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "DB results", Toast.LENGTH_SHORT).show();
        }*/
        adapter.updatePage(data.getQueryIdentifier(), data.getImageResults());
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (imageListPresenter != null) {
            imageListPresenter.detachView();
        }
    }

    @Override
    public void onNextPage() {
        if (imageListPresenter != null) {
            imageListPresenter.next();
        }
    }

    @Override
    public void onImageResultSelected(ImageResult imageResult) {
        startActivity(ImageActivity.newIntent(this, imageResult.getLink()));
    }
}
