package com.yossisegev.yossisegevdemoapp.ui;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by Yossi Segev on 14/12/2016.
 */

public class PagedRecyclerView extends RecyclerView {

    private static final int VISIBLE_THRESHOLD = 1;
    private LinearLayoutManager mLayoutManager;
    private PagingListener mPagingListener;

    public PagedRecyclerView(Context context) {
        super(context);
        init();
    }

    public PagedRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PagedRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        mLayoutManager = new LinearLayoutManager(getContext());
        setLayoutManager(mLayoutManager);
        addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if ((totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + VISIBLE_THRESHOLD)) {

                    // End has been reached
                    if (mPagingListener != null) {
                        mPagingListener.onNextPage();
                    }
                }
            }
        });
    }

    public void setPagingListener(PagingListener pagingListener) {
        mPagingListener = pagingListener;
    }

    public interface PagingListener {
        void onNextPage();
    }
}
