package com.yossisegev.yossisegevdemoapp.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.yossisegev.yossisegevdemoapp.R;

public class ImageActivity extends AppCompatActivity {

    private static final String EXTRA_IMAGE_URL = "extra:url";

    public static Intent newIntent(Context context, String imageUrl) {
        Intent i = new Intent(context, ImageActivity.class);
        i.putExtra(EXTRA_IMAGE_URL, imageUrl);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        String url = getIntent().getStringExtra(EXTRA_IMAGE_URL);
        if (TextUtils.isEmpty(url)) {
            finish();
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        ImageView imageView = (ImageView) findViewById(R.id.activity_image_image_view);
        Picasso.with(this).load(url).into(imageView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
