package com.yossisegev.yossisegevdemoapp.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;
import com.yossisegev.yossisegevdemoapp.R;
import com.yossisegev.yossisegevdemoapp.common.App;
import com.yossisegev.yossisegevdemoapp.common.PagedImageList;
import com.yossisegev.yossisegevdemoapp.entities.ImageResult;
import com.yossisegev.yossisegevdemoapp.entities.QueryIdentifier;

import java.util.List;

/**
 * Created by Yossi on 12/14/16.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {

    private Context context;
    private PagedImageList images;
    private Listener listener;
    public ImageAdapter(Context context, Listener listener) {
        images = new PagedImageList();
        this.listener = listener;
        this.context = context;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_adapter_row, parent, false);
        return new ImageViewHolder(view);
    }

    public void updatePage(QueryIdentifier queryIdentifier, List<ImageResult> newImageResults) {
        images.updatePage(queryIdentifier, newImageResults);
        notifyDataSetChanged();
    }
    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        final ImageResult imageResult = images.get(position);
        holder.title.setText(imageResult.getTitle());
        Picasso.with(context).load(imageResult.getThumbnail()).into(holder.image);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onImageResultSelected(imageResult);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public void clear() {
        images.clear();
        notifyDataSetChanged();
    }

    public interface Listener {
        void onImageResultSelected(ImageResult imageResult);
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView image;
        public ImageViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.image_adapter_row_title);
            image = (ImageView) itemView.findViewById(R.id.image_adapter_row_image);
        }
    }
}
