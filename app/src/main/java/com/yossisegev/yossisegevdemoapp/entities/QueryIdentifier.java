package com.yossisegev.yossisegevdemoapp.entities;

/**
 * Created by Yossi Segev on 15/12/2016.
 */

public class QueryIdentifier {

    private String id;
    private String searchTerm;
    private int startIndex;
    private int resultsCount;

    public QueryIdentifier(String searchTerm, int startIndex, int resultsCount) {
        this.searchTerm = searchTerm;
        this.startIndex = startIndex;
        this.resultsCount = resultsCount;
        this.id = searchTerm + startIndex + resultsCount;
    }

    public String getId() {
        return id;
    }

    public QueryIdentifier getNextIdentifier() {
        return new QueryIdentifier(searchTerm,
                (startIndex + resultsCount),
                resultsCount);
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getResultsCount() {
        return resultsCount;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof QueryIdentifier) {
            QueryIdentifier other = (QueryIdentifier) obj;
            if (other.getId().equals(getId())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return 53 * hash + getId().hashCode();
    }
}
