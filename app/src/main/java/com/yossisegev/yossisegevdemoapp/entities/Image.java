package com.yossisegev.yossisegevdemoapp.entities;

/**
 * Created by Yossi Segev on 15/12/2016.
 */

public class Image {

    private String thumbnailLink;

    public String getThumbnailLink() {
        return thumbnailLink;
    }

    public Image() {
    }

    public Image(String thumbnailLink) {
        this.thumbnailLink = thumbnailLink;
    }
}
