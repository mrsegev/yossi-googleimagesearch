package com.yossisegev.yossisegevdemoapp.entities;

/**
 * Created by Yossi Segev on 15/12/2016.
 */

public class Request {

    private int count;
    private int startIndex;
    private String searchTerms;

    public int getCount() {
        return count;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public String getSearchTerms() {
        return searchTerms;
    }
}
