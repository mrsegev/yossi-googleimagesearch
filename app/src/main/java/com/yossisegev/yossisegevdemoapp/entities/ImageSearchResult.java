package com.yossisegev.yossisegevdemoapp.entities;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * Created by Yossi on 12/14/16.
 */

public class ImageSearchResult {

    public static final int SOURCE_DB = 1;
    public static final int SOURCE_API = 2;
    private int source;
    private QueryIdentifier queryIdentifier;
    private int nextIndex;
    private List<ImageResult> imageResults;

    public ImageSearchResult(int source, @NonNull QueryIdentifier queryIdentifier, @NonNull List<ImageResult> imageResults) {
        this.source = source;
        this.imageResults = imageResults;
        this.queryIdentifier = queryIdentifier;
    }

    public int getSource() {
        return source;
    }

    public List<ImageResult> getImageResults() {
        return imageResults;
    }

    public QueryIdentifier getQueryIdentifier() {
        return queryIdentifier;
    }

    public int getNextIndex() {
        return nextIndex;
    }

    public void setNextIndex(int nextIndex) {
        this.nextIndex = nextIndex;
    }
}
