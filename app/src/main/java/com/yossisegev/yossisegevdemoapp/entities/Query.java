package com.yossisegev.yossisegevdemoapp.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Yossi Segev on 15/12/2016.
 */

public class Query {

    @SerializedName("request")
    private List<Request> currentRequest;
    @SerializedName("nextPage")
    private List<Request> nextRequest;

    /*
        Because of time constraints,
        I'm assuming all list are not empty and with the size of 1.
        -Yossi
     */
    public Request getCurrentRequest() {
        return currentRequest.get(0);
    }

    public Request getNextRequest() {
        return nextRequest.get(0);
    }

    public QueryIdentifier getIdentifier() {
        Request req = currentRequest.get(0);
        return new QueryIdentifier(req.getSearchTerms(), req.getStartIndex(), req.getCount());
    }
}
