package com.yossisegev.yossisegevdemoapp.entities;

/**
 * Created by Yossi on 12/14/16.
 */

public class ImageResult {

    private String id;
    private String title;
    private String link;
    private Image image;

    public static ImageResult mock(String id) {
        ImageResult imageResult = new ImageResult();
        imageResult.setId(id);
        imageResult.setTitle("title_" + id);
        imageResult.setLink("link_" + id);
        imageResult.setThumbnail("thumb_" + id);
        return imageResult;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    public String getThumbnail() {
        return image.getThumbnailLink();
    }

    public void setThumbnail(String thumbnail) {
        image = new Image(thumbnail);
    }

    public String getTitle() {
        return title;
    }
}
