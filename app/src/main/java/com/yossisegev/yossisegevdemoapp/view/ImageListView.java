package com.yossisegev.yossisegevdemoapp.view;

import com.yossisegev.yossisegevdemoapp.entities.ImageSearchResult;

/**
 * Created by Yossi Segev on 14/12/2016.
 */

public interface ImageListView extends BaseView {
    void onError();
    void showLoading(boolean show);
    void clearList();
    void onImageResponseData(ImageSearchResult data);
}
