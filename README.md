# README #

**Architecture**

MVP architecture.

It's simple and powerful and it helps me separate the different layers of my app.

This allows me to write a cleaner, more maintainable and more "testable" code.



**REST calls**

I used Retrofit2 as my rest client.

It’s powerful and backed by some of the best Android dev’s. I can trust it completely.

If i’ll ever need to customize my "networking needs” I can always customize the underling OkHttp client.



**Local DB**

Using Sqlite. Access by Loader to allow async reading and writing.



**Testing**

I wrote test around the DB functionality and around the custom list implementation.


I hope this readme file covers everything it should. 
I'll be more than happy to discuss anything I might have missed. Thanks :)